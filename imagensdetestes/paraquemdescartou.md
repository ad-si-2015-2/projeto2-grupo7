**Quando Jogador Descarta**
Quando jogador selecionar comando de descartar. 
O jogador deve selecionar o comando e a carta a ser descartada.
Recebe retorno das as cartas que jogador ainda tem. 
Recebe retorno do jogador da próxima vez.