**Iniciar Jogo**
Depois do servidor iniciado, 
solicita o nome do jogador (cliente), 
retorna que jogador ele é e se pode iniciar o jogo.
E ao dar o comando para iniciar o jogo, são distribuídas e mostradas as suas cartas.