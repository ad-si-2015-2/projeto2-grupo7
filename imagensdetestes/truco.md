**Truco!**
Trucar é um dos comandos possíveis do jogo.
Quando um jogador selecionar em sua vez os outros recebem mensagem que ele trucou e que jogador foi.
O próximo jogador da vez pode selecionar correr ou aceitar, e receber a carta descartada do jogador que trucou.