package Truco;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface Cliente extends Remote {
    
    public void setDupla(int numDupla) throws RemoteException;
    
    public void notifica(String mensagem) throws RemoteException;
    
    public String getNome() throws RemoteException;
    
    public void joga() throws RemoteException;
    
    public void solicitaResposta(String mensagem) throws RemoteException;
    
    public void setBaralho(ArrayList<Carta> baralho) throws RemoteException;
    
    public void imprimeCartas() throws RemoteException;
    
    public void imprimeComandos() throws RemoteException;
    
    public int getDupla() throws RemoteException;
    
    public ArrayList<Carta> getBaralho() throws RemoteException;
}
