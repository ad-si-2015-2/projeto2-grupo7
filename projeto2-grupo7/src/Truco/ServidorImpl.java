package Truco;

import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServidorImpl implements Servidor {

    private ArrayList<Cliente> jogadores = new ArrayList<Cliente>();
    private String ultimaCartaJogada = null;
    private Baralho baralho = new Baralho();
    private int atualDuplaVencedora;
    private int pontosDupla1 = 0;
    private int pontosDupla2 = 0;
    private boolean jogadaTrucada = false;
    private boolean jogadaTrucadaAceite = false;
    private int duplaTrucou = -1;
    private int vez = 0;
    private int valorAposta = 1;
    private boolean jogoIniciado = false;
    private boolean jogoFinalizado = false;

    //Método para registrar jogador no jogo
    @Override
    public void registrar(Cliente cliente) throws RemoteException {
        solicitaConexao(cliente);

        if (jogoPodeIniciar()) {
            broadCast("O jogador 1 pode iniciar o jogo...");
            jogadores.get(0).solicitaResposta("Digite .iniciar para comecar o jogo...");
            iniciaJogo();
        }
    }

    public void solicitaConexao(Cliente jogador) throws RemoteException {
        if (jogadores.size() < 4) {
            jogadores.add(jogador);
            //jogadores ímpares ficam na dupla 1, jogadores pares na 2
            if ((jogadores.size() % 2) != 0) {
                jogador.setDupla(1);
            } else {
                jogador.setDupla(2);
            }
            jogador.notifica("Bem-vindo! Voce eh o(a) jogador(a) " + getNumeroJogadores());
        } else {
            jogador.notifica("Mesa cheia! :(");
        }
    }

    public void iniciaJogo() throws RemoteException {
        distribuiCartas();
        for (int i = 0; i < jogadores.size(); i++) {
            jogadores.get(i).imprimeCartas();
            jogadores.get(i).imprimeComandos();
        }
        joga();
    }

    private void joga() {
        try {
            vez = 0; //a vez é do jogador 1
            Cliente jogador;
            while (!jogoFinalizado) {
                jogador = jogadores.get(vez);
                notificaTodosMenosUm(vez, "Vez de " + jogador.getNome());
                jogador.solicitaResposta("\nDigite um comando...");
                if (vez == jogadores.size() - 1) {
                    calculaVencedorRodada();
                    if(isFimDeJogo()) {
                        fimJogo();
                    }
                    vez = 0;
                    iniciaJogo();
                } else {
                    vez++;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ClienteImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean isFimDeJogo() {
        if (pontosDupla1 >= 11 || pontosDupla2 >= 11) {
            return true;
        } else {
            return false;
        }
    }

    private void calculaVencedorRodada() throws RemoteException {
        broadCast("\n\n\n\n\t**VENCEDORES DA RODADA: Dupla " + atualDuplaVencedora + "**");
        if (atualDuplaVencedora == 1) {
            pontosDupla1 = pontosDupla1 + valorAposta;
        } else {
            pontosDupla2 = pontosDupla2 + valorAposta;
        }
        broadCast("\tPONTOS DUPLA 1: " + pontosDupla1 + "\tPONTOS DUPLA 2: " + pontosDupla2);
        broadCast("\n\n\n");
        baralho.embaralha();
    }

    public void notificaTodosMenosUm(int indice, String mensagem) throws RemoteException {
        for (int i = 0; i < jogadores.size(); i++) {
            if (i == indice) {
                continue;
            } else {
                jogadores.get(i).notifica(mensagem);
            }
        }
    }

    //Verifica se o jogo pode iniciar
    //só inicia quando há quatro jogadores
    @Override
    public boolean jogoPodeIniciar() {
        if (jogadores.size() == 4) {
            return true;
        } else {
            return false;
        }
    }

    public boolean vezDeJogar(Cliente jogador) throws RemoteException {
        if (jogador.getNome().equals(jogadores.get(vez).getNome())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean iniciar() throws RemoteException {
        if (jogoIniciado) {
            return false;
        }
        if (!jogoPodeIniciar()) {
            return false;
        }

        jogoIniciado = true;
        baralho.embaralha();

        return true;
    }

    public boolean pontuacao() throws RemoteException {
        jogadores.get(vez).notifica("\nPontos dupla 1: " + pontosDupla1 + "\nPontos dupla 2: " + pontosDupla2);
        jogadores.get(vez).notifica("\nA sua dupla eh a " + jogadores.get(vez).getDupla());
        vez--;
        return true;
    }

    public boolean descartar(String carta) throws RemoteException {
        if (!vezDeJogar(jogadores.get(vez))) {
            jogadores.get(vez).notifica("Nao eh sua vez de jogar!");
            return false;
        }

        if (jogadaTrucada && !jogadaTrucadaAceite) {
            jogadores.get(vez).notifica("A jogada estah trucada, eh necessário .correr ou .aceitar para continuar!");
            return false;
        }

        registrarJogada(jogadores.get(vez), carta);
        return true;
    }

    public boolean registrarJogada(Cliente jogador, String carta) throws RemoteException {

        boolean jogadorPossuiCarta = false;

        int indiceCarta = 0;

        int valorCartaJogador = 0;

        ArrayList<Carta> baralhoJogador = jogador.getBaralho();

        for (Carta cartaJogador : baralhoJogador) {
            if (cartaJogador.getSimbolo().equals(carta)) {
                jogadorPossuiCarta = true;
                valorCartaJogador = cartaJogador.getValor();
                baralhoJogador.remove(indiceCarta);
                jogador.setBaralho(baralhoJogador);
                break;
            }
            indiceCarta++;
        }

        if (!jogadorPossuiCarta) {
            jogador.notifica("\nVoce nao possui essa carta! Tente outra...\n");
            vez--;
            return false;
        }

        if (ultimaCartaJogada == null) {
            atualDuplaVencedora = jogador.getDupla();
        } else {
            int i = 0;
            for (Carta cartaBaralho : baralho.getBaralho()) {
                if (cartaBaralho.getSimbolo().equals(ultimaCartaJogada)) {
                    if (valorCartaJogador > cartaBaralho.getValor()) {
                        atualDuplaVencedora = jogador.getDupla();
                    }
                }
            }
        }

        notificaTodosMenosUm(vez, "\nCarta descartada: " + carta);

        jogador.imprimeCartas();

        return true;
    }

    private void fimRodada() {
        jogadaTrucada = false;
        duplaTrucou = -1;
        valorAposta = 1;
    }

    private void fimJogo() throws RemoteException {
        setJogoFinalizado(true);
        broadCast("Fim de jogo!");
        broadCast("\n\n\n\n\t****DUPLA VENCEDORA:****");
        if(pontosDupla1 >= 11) {
        broadCast("\t****DUPLA 1!****");  
        } else  {
            broadCast("\t****DUPLA 2!****");
        }
    }

    public boolean trucar() throws RemoteException {
        if (!vezDeJogar(jogadores.get(vez))) {
            jogadores.get(vez).notifica("Nao eh sua vez de jogar!");
            return false;
        }

        /*     if (jogadaTrucada = true) {
            jogadores.get(vez).notifica("A rodada ja estah trucada!");
            return false;
        }*/
        jogadaTrucada = true;
        duplaTrucou = vez;
        //valorAposta = 3;
        notificaTodosMenosUm(vez, "A rodada foi trucada pelo jogador: " + jogadores.get(vez).getNome() + "!");
        return true;
    }

    public boolean aceitar() throws RemoteException {
        if (!vezDeJogar(jogadores.get(vez))) {
            jogadores.get(vez).notifica("Nao eh sua vez de jogar!");
            return false;
        }

        jogadaTrucadaAceite = true;

        notificaTodosMenosUm(vez, "O truco foi aceito!");

        jogadores.get(vez).notifica("Voce aceitou o truco!");
        vez = vez - 2;
        valorAposta = 3;
        jogadaTrucadaAceite = false;
        jogadaTrucada = false;
        return true;
    }

    public boolean correr() throws RemoteException {
        if (!vezDeJogar(jogadores.get(vez))) {
            jogadores.get(vez).notifica("Nao eh sua vez de jogar!");
            return false;
        }

        jogadaTrucada = false;
        duplaTrucou = -1;
        valorAposta = 1;

        if (vez == 0 || vez == 2) {
            pontosDupla2 += valorAposta;
        } else {
            pontosDupla1 += valorAposta;
        }

        validarPontosDuplas();
        fimRodada();
        return true;
    }

    private void validarPontosDuplas() throws RemoteException {
        if (pontosDupla1 >= 11) {
            broadCast("A dupla 1 venceu!");
            fimJogo();
        } else if (pontosDupla2 >= 11) {
            broadCast("A dupla 2 venceu!");
            fimJogo();
        }
    }

    private void broadCast(String mensagem) throws RemoteException {
        for (Cliente jogador : jogadores) {
            jogador.notifica(mensagem);
        }
    }

    public String[] separarComandoDescartar(String comando) {
        String[] resultado = comando.split(" ");
        return resultado;
    }

    private String removeBackSpace(String comando) {

        ArrayList<Character> caracteresValidos = new ArrayList<>();
        Character[] caracteresOriginais = new Character[comando.length()];

        int i = 0;
        for (char c : comando.toCharArray()) {
            caracteresOriginais[i++] = c;
        }
        caracteresValidos.addAll(Arrays.asList(caracteresOriginais));

        for (int k = 0, j = 0; k < comando.length(); k++) {
            if (caracteresOriginais[k] == '\b') {
                caracteresValidos.remove(k - j++);
                if (k - j + 1 > 0) {
                    caracteresValidos.remove(k - j++);
                }
            }
        }
        char[] chars = new char[caracteresValidos.size()];
        for (int z = 0; z < chars.length; z++) {
            chars[z] = caracteresValidos.get(z);
        }

        return String.valueOf(chars);
    }

    public static void main(String args[]) {
        try {
            ServidorImpl obj = new ServidorImpl();
            Servidor stub = (Servidor) UnicastRemoteObject.exportObject(obj, 0);

            Registry registry = LocateRegistry.createRegistry(6789);
            registry.bind("Jogo", stub);

            System.out.println("Servidor iniciado...");

        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public int getNumeroJogadores() throws RemoteException {
        return jogadores.size();
    }

    @Override
    public void distribuiCartas() throws RemoteException {
        int i = 0;

        for (Cliente jogador : jogadores) {
            jogador.setBaralho(new ArrayList<Carta>(baralho.getBaralho().subList(i, (i + 3))));
            i = i + 3;
        }
    }

    @Override
    public boolean isJogoFinalizado() {
        return jogoFinalizado;
    }

    public void setJogoFinalizado(boolean jogoFinalizado) {
        this.jogoFinalizado = jogoFinalizado;
    }

    @Override
    public boolean interpretadorComandos(String comando, Cliente jogador) throws RemoteException {

        if (!vezDeJogar(jogador)) {

            jogadores.get(vez).notifica("Nao eh sua vez de jogar!");
            return false;
        }
        String[] comandoDelimitado = separarComandoDescartar(comando);
        comando = removeBackSpace(comando);

        switch (comando) {
            case ".iniciar":
                if (jogoIniciado) {
                    jogador.notifica("\nJogo ja iniciado!\n"
                            + "Digite um comando...\n");
                }
                return iniciar();
            case ".pontuacao":
                return pontuacao();
            case ".trucar":
                return trucar();
            case ".aceitar":
                return aceitar();
            case ".correr":
                return correr();
            default:
                String[] comandoQuebrado = separarComandoDescartar(comando);
                if (!comandoQuebrado[0].equals(".descartar") || comandoQuebrado[1].length() > 2) {
                    jogador.notifica("\nComando invalido!\n");
                    return false;
                } else {
                    return descartar(comandoQuebrado[1]);
                }
        }
    }
}
