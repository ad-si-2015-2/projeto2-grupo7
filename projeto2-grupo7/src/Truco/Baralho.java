package Truco;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class Baralho implements Serializable{

    private ArrayList<Carta> baralho = new ArrayList<Carta>();

    public Baralho() {
        baralho.add(new Carta("Dama de Ouro", "QO", 1));
        baralho.add(new Carta("Dama de Espada", "QE", 1));
        baralho.add(new Carta("Dama de Copas", "QC", 1));
        baralho.add(new Carta("Dama de Paus", "QP", 1));
        
        baralho.add(new Carta("Valete de Ouro", "VO", 2));
        baralho.add(new Carta("Valete de Espada", "VE", 2));
        baralho.add(new Carta("Valete de Copas", "VC", 2));
        baralho.add(new Carta("Valete de Paus", "VP", 2));
        
        baralho.add(new Carta("Rei de Ouro", "KO", 3));
        baralho.add(new Carta("Rei de Espada", "KE", 3));
        baralho.add(new Carta("Rei de Copas", "KC", 3));
        baralho.add(new Carta("Rei de Paus", "KP", 3));
        
        baralho.add(new Carta("As de Ouro", "AO", 4));
        baralho.add(new Carta("As de Copas", "AC", 4));
        baralho.add(new Carta("As de Paus", "AP", 4));
        
        baralho.add(new Carta("2 de Ouro", "2O", 5));
        baralho.add(new Carta("2 de Espada", "2E", 5));
        baralho.add(new Carta("2 de Copas", "2C", 5));
        baralho.add(new Carta("2 de Paus", "2P", 5));
        
        baralho.add(new Carta("3 de Ouro", "3O", 6));
        baralho.add(new Carta("3 de Espada", "3E", 6));
        baralho.add(new Carta("3 de Copas", "3C", 6));
        baralho.add(new Carta("3 de Paus", "3P", 6));
        
        baralho.add(new Carta("7 de Ouro", "7O", 7));
        baralho.add(new Carta("As de Espada", "AE", 8));
        baralho.add(new Carta("7 de Copas", "7C", 9));
        baralho.add(new Carta("4 de Paus", "4P", 10));
    }
    
    public ArrayList<Carta> getBaralho() {
        return this.baralho;
    }
    
    public void embaralha() {
        Collections.shuffle(baralho);
    }
    
    public Carta getCarta(String simboloCarta)
    {
        for (Carta carta : baralho) {
            if (carta.getSimbolo() == null ? simboloCarta == null : carta.getSimbolo().equals(simboloCarta))
            {
                return carta;
            }
        }
         return null;
    }
}
