package Truco;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface Servidor extends Remote {

    public void registrar(Cliente cliente) throws RemoteException;
    
    public int getNumeroJogadores() throws RemoteException;
    
    public void distribuiCartas() throws RemoteException;
    
    public boolean isJogoFinalizado() throws RemoteException;
    
    public boolean interpretadorComandos(String comando, Cliente jogador) throws RemoteException;
    
    public boolean jogoPodeIniciar() throws RemoteException;
}
