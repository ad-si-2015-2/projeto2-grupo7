package Truco;


import java.util.ArrayList;

public class Teste {
public static void main (String args[]) {
    ArrayList<String> teste = new ArrayList<String>();
    
    teste.add("1");
    teste.add("2");
    teste.add("3");
    teste.add("4");
    teste.add("5");
    teste.add("6");
    teste.add("7");
    
    System.out.println("Tamanho: " + teste.size());
    
    teste.remove("5");
    
    System.out.println("Tamanho: " + teste.size());
    
    teste.remove(0);
    
    System.out.println("Tamanho: " + teste.size());
}
}
