package Truco;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClienteImpl implements Cliente {

    private String nome;
    private ArrayList<Carta> baralho;
    private int dupla;
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private static Registry registry;
    private static Cliente jogador;
    private static Servidor servidor;
    private static ClienteImpl cliente;

    List<Integer> cartasJogador; // variável para armazenar as cartas do jogador
    int batedor = 0; // variável que indica se o jogador bateu
    int ordem; // variável que armazena a ordem de jogo de cada jogador

    static {
        try {
            registry = LocateRegistry.getRegistry("localhost", 6789);
            servidor = (Servidor) registry.lookup("Jogo");

        } catch (RemoteException ex) {
            System.err.println("Nao foi possivel conectar ao servidor. Servidor esta ativo?");
            System.err.println(ex);
            System.exit(0);
        } catch (NotBoundException ex) {
            System.err.println("Nao foi possivel encontrar o registro. Variavel esta correta?");
            System.err.println(ex);
            System.exit(0);
        }
    }

    public void registra(Cliente cliente) throws RemoteException {
        servidor.registrar(cliente);
    }

    @Override
    public void solicitaResposta(String mensagem) {

        notifica(mensagem);

        boolean resposta = false;
        String comando = "";

        do {
            try {
                comando = reader.readLine();
                resposta = servidor.interpretadorComandos(comando, this);
            } catch (Exception ex) {
                Logger.getLogger(ClienteImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (!resposta);
    }

    @Override
    public void imprimeCartas() throws RemoteException {
        System.out.println("\nAs suas cartas sao: ");
        for (Carta carta : baralho) {
            System.out.print(carta.getSimbolo() + " | ");
        }
        System.out.println();
    }

    public void setBaralhoJogador(ArrayList<Carta> baralho) {
        this.baralho = baralho;
    }

    public void addCartaBaralho(Carta carta) {
        baralho.add(carta);
    }

    @Override
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public ArrayList<Carta> getBaralho() throws RemoteException{
        return baralho;
    }

    @Override
    public void setBaralho(ArrayList<Carta> baralho) throws RemoteException {
        this.baralho = baralho;
    }

    @Override
    public int getDupla() throws RemoteException{
        return dupla;
    }

    public void setDupla(int numDupla) {
        this.dupla = numDupla;
    }

    @Override
    public void imprimeComandos() throws RemoteException{
        System.out.println("Comandos disponiveis: \n"
                + "\t .iniciar \n"
                + "\t .pontuacao \n"
                + "\t .descartar (carta) \n"
                + "\t .trucar \n"
                + "\t .aceitar \n"
                + "\t .correr \n");
    }

    @Override
    public void notifica(String mensagem) {
        System.out.println(mensagem);
    }

    //chamar ao iniciar a partida
    @Override
    public void joga() {
        String comando = null;
        boolean resposta = false;
        try {
            System.out.println("O jogo comecou!");
            servidor.distribuiCartas();

            while (!servidor.isJogoFinalizado()) {
                imprimeCartas();
                imprimeComandos();
                System.out.println("Digite um comando...");

                do {
                    comando = reader.readLine();

                    resposta = servidor.interpretadorComandos(comando, this);

                    if (!resposta) {
                        System.out.println("Comando invalido!");
                        imprimeComandos();
                    }
                    
                } while (!resposta);
            }
        } catch (Exception e) {
            Logger.getLogger(ClienteImpl.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public static void main(String[] args) throws Exception {

        String host = (args.length < 1) ? null : args[0];

        cliente = new ClienteImpl();
        System.out.println("Bem vindo!");
        System.out.println("Favor, digite seu nome:");
        String nomeJogador = reader.readLine();
        cliente.setNome(nomeJogador);

        System.out.println("A partir daqui ler os retornos dos objetos de controle!");
        try {
            Cliente stub = (Cliente) UnicastRemoteObject.exportObject(cliente, 0);
            Random gerador = new Random();
            Registry registry = LocateRegistry.createRegistry(gerador.nextInt(1000) + 6000);
            registry.bind("Cliente", stub);
            cliente.registra(cliente);

        } catch (Exception e) {
            System.out.println("Ocorreu um erro na conexao com o servidor!" + e.getStackTrace());
        }
    }
}
