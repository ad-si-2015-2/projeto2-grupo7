package Truco;


import java.io.Serializable;

public class Carta implements Serializable{
    
    private String nome;
    private int valor;
    private String simbolo;
    
    public Carta (String nome, String simbolo, int valor) {
        this.nome = nome;
        this.simbolo = simbolo;
        this.valor = valor;
    }
    
    public int getValor() {
        return this.valor;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public String getSimbolo() {
        return this.simbolo;
    }
}
